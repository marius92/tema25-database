<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BooksRepository")
 */
class Books
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nume;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $autor;

    /**
     * @ORM\Column(type="integer")
     */
    private $anul;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $publicatie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getAutor(): ?string
    {
        return $this->autor;
    }

    public function setAutor(string $autor): self
    {
        $this->autor = $autor;

        return $this;
    }

    public function getAnul(): ?int
    {
        return $this->anul;
    }

    public function setAnul(int $anul): self
    {
        $this->anul = $anul;

        return $this;
    }

    public function getPublicatie(): ?string
    {
        return $this->publicatie;
    }

    public function setPublicatie(string $publicatie): self
    {
        $this->publicatie = $publicatie;

        return $this;
    }
}
