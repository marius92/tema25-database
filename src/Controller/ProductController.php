<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        $category = new Category();
        $category->setName('Computer Peripherals');


        $entityManager = $this->getDoctrine()->getManager();
        $product = new Product();
        $product->setName("Tastatura");
        $product->setPrice(200);
        $product->setDescription("De la Altex");

        $product->setCategory($category);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($category);

        $entityManager->persist($product);
        $entityManager->flush();


        return $this->render('product/index.html.twig', [
            'controller_name' => $product->getId() . $category->getId(),
        ]);
    }

    /**
     * @Route("/product/show/{id}", name="show")
     */
    public function show($id)
    {
        // $categoryName = $product->getCategory()->getName();
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (!$product) {
            throw $this->createNotFoundException('No product found for id ' . $id);
        }
        return $this->render('product/index.html.twig', ['controller_name' => $product->getName() . $product->getCategory->getName()]);
    }

    /**
     * @Route("/product/update/{id}", name="update")
     */

    public function update($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);
        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id: ' . $id
            );
        }
        $product->setName('New product name!');
        $entityManager->flush();
        //redirectionez acum la show dupa ce am facut update- folosescname="show"
        return $this->redirectToRoute('show', [
            'id' => $product->getId()
        ]);
    }
    /**
     * @Route("/product/delete/{id}", name="delete")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException("Produsul " . $id . " este deja sters");
        }
        $entityManager->remove($product);
        $entityManager->flush();

        return $this->redirectToRoute('show', ['id' => $product->getId()]);
    }

    /**
     * @Route("/category/{id}", name="category")
     */
    public function category($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($id);
        if (!$category) {
            throw $this->createNotFoundException(
                'No category found on id ' . $id
            );
            $products = $category->getProducts();
        }
        return dump($products);
    }
}
