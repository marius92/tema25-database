<?php

namespace App\Controller;

use App\Entity\Books;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class BooksController extends AbstractController
{
    /**
     * @Route("/books", name="books")
     */
    public function new(Request $request)
    {
        $books = new Books();

        $form = $this->createFormBuilder($books)
            ->add('nume', TextType::class)
            ->add('autor', TextType::class)
            ->add('anul', IntegerType::class)
            ->add('publicatie', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Books'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $books = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($books);
            $entityManager->flush();

            return new Response('Books created successfully!');
        }

        return $this->render('books/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    public function __toString()
    {
        return (string)$this->nume;
    }
}
